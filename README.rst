G15DPMS-MONITOR
===============

A simple 'screensaver' tool for g15daemon and the Logitech G15.

The tool simply waits in the background for the monitor to be put into
standby by a screensaver via DPMS.  When this happens, it turns all the keyboard
lights off, restoring them when the monitor re-activates.

=======
Warning
=======
I'm discontinuing this after someone made a fuzz about a feature he decided he want a decade later. 
And as far I am concerned, Arch Linux's AUR administrators find this behavior just fine, so I'm not wasting my efforts on this anymore.
I can still fix issues as I always did and help via mail, but keep in mind Arch Linux is impossible to be supported.

=====
USAGE
=====

Without any commandline options, g15dpms-monitor will simply wait for DPMS to become activated (forcing the monitor into standby mode), and switch off keyboard lights.

When using the --cmd-enable <program> commandline option, g15dpms-monitor will run the specified program when the monitor goes into Standby mode.

When --cmd-disable <program> is used, the program specified will be run when the monitor re-activates.

--help shows available options
